# selfhosted-spec

In this repository, you will find tests for [ansible-selfhosted](https://codeberg.org/southerntofu/ansible-selfhosted). In the future, documentation and specifications for the different roles will live in this repository.

The tests in here rely on LXC to start containers to run the tests. The `setup/host.sh` script will turn a fresh Debian setup into a testing server. **WARNING:** it will heavily change your system configuration, and in particular the network by setting up a bridge interface for the containers to access your LAN.

# Setup

Usually, you want to download this repository as a submodule of ansible-selfhosted. In that case, just do `mkdir /root/Prog && git clone --recursive https://codeberg.org/southerntofu/ansible-selfhosted /root/ansible-selfhosted`. **The test runner will automatically detect ansible-selfhosted recipes when they are located in /root/Prog/**.

Otherwise, if you already have copy you'd like to keep, you can clone the repository anywhere, then follow these two simple steps:

1. configure `config.env` by renaming `config.default.env` and adapting it to your local setup
2. run `setup/host.sh` (**WARNING: this should be run only on a machine dedicated to tests unless you know what you are doing**)

# Running tests

The tests are located in the `tests/` folder. Each test has a yml ansible-selfhosted config file, and a corresponding shell script that will get executed if the setup succeeds, to determine whether the server is properly configured. This test script is executed from the host (an external client) and has the "ip" environment variable pointing to the container to test.

You can run the tests with `test.sh` by passing explicit test scripts (eg. `./test.sh tests/*.sh` ). If you don't pass it any test, it will find all tests in the tests/ folder. For additional debugging info, you can use the `--debug` flag.

# TODO

- [ ] Isolation and parallelization
  - [ ] Number of threads and quantity of RAM per container
  - [ ] Number of tests to run in parallel
  - [ ] Run tests in parallel in separate containers (one per test, for bootstrappability)
  - [ ] Run tests sequentially in the same container (check for breakage due to changing config over time)
- [ ] Archlinux support
  - [ ] Get archlinux LXC template to boot ([this bug](https://github.com/lxc/lxc/issues/642): requires pacman on host)
  - [ ] Write the stuff in `distro/archlinux.sh`
- [ ] Make tests work fully locally without Internet connection
  - [x] Autodiscover apt/arch package caching proxy (done for apt)
  - [ ] Serve well-known git repositories from host (git proxy?)
  - [ ] Setup [pebble](https://github.com/letsencrypt/pebble) ACME server for TLS setup
  - [ ] For tor it's a lot more complicated, what should we do?
  - [ ] There's a bunch of stuff we download from ansible (git/https), what should we do?
- [ ] UX
  - [x] Color output
  - [x] Display short test output but save long stuff to logfile
  - [x] Request specific tests via CLI arguments
