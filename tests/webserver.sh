#! /usr/bin/env bash

echo "Trying webserver tests to $CONTAINERIP ($CONTAINERNAME)"

HAS_FAILED=0

function indent() {
	echo "$@" | sed 's/^/    /g'
}

# curlGrep MSG HOST QUERY GREP
function curlGrep() {
	MSG="$1"
	HOST="$2"
	QUERY="$3"
	GREP="$4"
	stderr="$(mktemp)"
	if [ ${TLS:-0} -eq 1 ]; then
		if [ ! -f /tmp/pebble-root.pem ]; then
			if ! curl --cacert /root/Prog/pebble/test/certs/pebble.minica.pem https://localhost:15000/roots/0 > /tmp/pebble-root.pem 2>"$stderr"; then
				echo "Failed to download pebble root CA... STDERR below:"
				cat "$stderr" | sed 's/^/    /g'
				return 1
			fi
		fi
		URL="https://"$HOST""$QUERY""
		OPT="--cacert /tmp/pebble-root.pem"
	else
		URL="http://"$HOST""$QUERY""
	fi
	reply="$(curl $OPT "$URL" 2>"$stderr")"
	if [ $? -eq 0 ]; then
		if echo "$reply" | grep -q "$GREP"; then
			echo "ok "$MSG""
			rm "$stderr"
			return 0
		else
			echo "fail "$MSG""
			echo "ERROR: Failed to find Copyleft section in ansible-selfhosted README. Got this instead:" | indent
			echo "$reply" | indent
		fi
	else
		echo "ERROR: Failed to reach $HOST on $CONTAINERIP"
		echo "  failed URL was "$URL". STDOUT below:"
		echo "$reply" | sed 's/^/    /g'
		echo "  STDERR below:"
		cat "$stderr" | sed 's/^/    /g'
	fi
	HAS_FAILED=1
	rm "$stderr"
}

curlGrep "static README.md" "test."$CONTAINERNAME"" "/README.md" "Copyleft (license)"
curlGrep "proxied apt-proxy" "testproxy."$CONTAINERNAME"" "/" "Apt-Cacher NG"
TLS=1 curlGrep "TLS static README.md" "testtls."$CONTAINERNAME"" "/README.md" "Copyleft (license)"

exit $HAS_FAILED
