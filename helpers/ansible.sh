#! /usr/bin/env bash

ANSIBLE_RETRY_FILES_ENABLED=0 ansible-playbook -e @$1 $2/main.yml -vv --connection=local --inventory 127.0.0.1, --limit 127.0.0.1
