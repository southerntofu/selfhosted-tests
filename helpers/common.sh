#! /usr/bin/env bash

export SRCDIR="/root/Prog"
if [ ! -d "$SRCDIR" ]; then
	mkdir "$SRCDIR"
fi

# Call the specific variant for the current distro
if grep -q "ID=debian" /etc/os-release; then
	source "$(dirname "$0")"/../distro/debian.sh
elif grep -q "ID=archlinux" /etc/os-release; then
	source "$(dirname "$0")"/../distro/archlinux.sh
else
	echo "ERROR: Unrecognized distro $(grep ID= /etc/os-release)"
	exit 1
fi

# Ensure a certain key equals a value in a config file
# $1 config file
# $2 key (you can indent it between quotes)
# $3 value (don't forget to escape quotes)
# $4 separator (" = " ? " " ? "=" ? default: " = ")
function ensure_config() {
    local SEP="${4:-" = "}"
    if grep -qP "^$2" "$1"; then
        # Entry found, check value
        if ! grep -qP "^$2$SEP$3$" "$1"; then
            echo "   [cfg] "$1" override "$2" to "$3""
            sed -i "s/^$2.*/$3/g" "$1"
        fi
    else
        # Entry not found, add it
        echo "    [cfg] "$1" set "$2" to "$3""
        echo "$2$SEP$3" >> "$1"
    fi
}

function setup_tor() {
	ensure_setup tor
        systemctl enable tor
        if ! grep -q "127.0.0.1:9050" /root/.gitconfig; then
                echo -e "\n[http]\n    proxy = socks5://127.0.0.1:9050\n[https]\n    proxy = socks5://127.0.0.1:9050" >> /root/.gitconfig
        fi
}

function setup_ansible_selfhosted() {
    ORIGDIR="$(pwd)"
    cd "$SRCDIR"

    if [ ! -d ansible-selfhosted ]; then
	if ! git clone --recursive https://codeberg.org/southerntofu/ansible-selfhosted; then
	    echo "ERROR: ansible-selfhosted download failed"
	    cd "$ORIGDIR"
	    exit 1
	fi
    else
	    cd ansible-selfhosted
	    git pull --recurse-submodules
	    cd ..
    fi

    cd "$ORIGDIR"
}

function ensure_network () {
	i="$1"
	if [[ "$i" == "0" ]]; then
		echo "Network timeout"
		return 1
	fi
	if getent hosts google.com; then
		# Network is working
		return 0
	else
		sleep 1
		remaining=$((i-1))
		ensure_network $remaining
		return $?
	fi
}
