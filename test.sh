#! /usr/bin/env bash

source "$(dirname "$0")"/helpers/colors.sh

DEBUG=0
STOPCONTAINERS=0

CONTAINERPREFIX="ansible"
ANSIBLEBASEDIR="/etc/ansible-selfhosted"

set -u
shopt -s nullglob
# We can capture exit code of command passed through pipe (eg. tee)
set -o pipefail
# EXTREME DEBUG MODE
#set -x

function help () {
	echo "Write some help message here..."
}

function debug() {
	if [ $DEBUG -eq 1 ]; then
		echo "$@"
	fi
}

function maybeStop() {
	if [ $STOPCONTAINERS -eq 1 ]; then
		doUnmount "${MOUNTPOINTS[@]}"
		lxc-stop "$CONTAINERNAME"
	fi
}

# eraseLines 5 erases the last 5 lines of output
# useful to update an area of text
eraseLines() {
	COUNT="$1"
	while [ $COUNT -gt 0 ]; do
		COUNT=$((COUNT-1))
		printf '\e[A\e[K'
	done
}

# runCMD CONTEXT CMDNAME COMMAND ADDITIONAL COMMAND FLAGS
#   Runs a command, saving output to a file while displaying the 5 last lines of output
#   as long as the command is running.
function runCMD() {
	CONTEXT="$1"
	NAME="$2"
	shift 2
	LOGFILE=/tmp/"$CONTEXT"."$NAME".log
	debug "("$CONTEXT") START CMD: "$NAME""
	# We start the command in the background, saving to $LOGFILE as it goes
	"$@" 2>&1 > "$LOGFILE" &
	PROC=$!
	debug "("$CONTEXT") Running $NAME, follow whis this command: tail -f \""$LOGFILE"\""
	while true; do
		if ps -p "$PROC" > /dev/null; then
			# process still running
			sleep 0.1
			continue
		fi
		break
	done
	# Wait will return the background process exit code
	wait $PROC
	SUCCESS=$?
	debug "($CONTEXT) $NAME exited with code $SUCCESS"
	if [ ! $SUCCESS -eq 0 ]; then
		RESULTS_CODE+=($SUCCESS)
		RESULTS_OUTPUT+=("$(cat "$LOGFILE")")
		return 1
	fi
	return 0
}


function doMount() {
	while [ $# -gt 0 ]; do
		local SRC="$1"
		local MOUNTPOINT="$2"
		shift 2
		if mount | grep -q "$MOUNTPOINT"; then
			# already mounted
			continue
		fi
		# TODO: maybe resolve symlinks?
		if [ -f "$SRC" ]; then
			if [ ! -f "$MOUNTPOINT" ]; then
				mkdir -p "$(dirname "$MOUNTPOINT")"
				touch "$MOUNTPOINT"
			fi
		elif [ -d "$SRC" ]; then
			if [ ! -d "$MOUNTPOINT" ]; then
				mkdir -p "$MOUNTPOINT"
			fi
		else
			red: "ERROR: Unsupported source type for "$SRC""
			return 1
		fi
		mount --bind -o ro "$SRC" "$MOUNTPOINT"
	done
}

function doUnmount() {
	while [ $# -gt 0 ]; do
		local MOUNTPOINT="$2"
		shift 2
		umount "$MOUNTPOINT"
	done
}

# $1: test name (to which $CONTAINERPREFIX will be prepended)
# $2: test file
# $3: test config
function runTest() {
	# Define common environment variables ; CONTAINERIP is defined below if setup is successful
	export CONTAINERNAME="$CONTAINERPREFIX"-"$1"
	local CONTAINERROOT="/var/lib/lxc/$CONTAINERNAME/rootfs"
	local TESTFILE="$2"
	local TESTCONFIG="$3"

	# Need to define them here, otherwide local var=bar erases exit code with 0 (success from 'local')
	local OUTPUT
	local SUCCESS

	# Hard abort on LXC errors
	set -e
	if [ ! -d "$CONTAINERROOT" ]; then
		if ! runCMD "$CONTAINERNAME" create lxc-create -t "$DISTRO" -n "$CONTAINERNAME"; then
			return 1
		fi
		if ! runCMD "$CONTAINERNAME" start lxc-start "$CONTAINERNAME"; then
			return 1
		fi
	elif lxc-ls -f | grep -qP ""$CONTAINERNAME" STOPPED"; then
		if ! runCMD "$CONTAINERNAME" restart lxc-start "$CONTAINERNAME"; then
			return 1
		fi
	fi
	set +e

	# Before starting things, we should update the dynamic configuration in server config
	# such as the host's IP address (useful for TLS certificate generation)
	mkdir -p /etc/ansible-selfhosted/testconfig
	TESTFINALCONFIG=/etc/ansible-selfhosted/testconfig/"$(basename "$TESTCONFIG")"
	envsubst < "$TESTCONFIG" > "$TESTFINALCONFIG"

	# Mount well-known folders read-only inside the container
	TARGETDIR="$CONTAINERROOT""$ANSIBLEBASEDIR"
	# Entries go in pairs: $1 is HOST $2 is CONTAINER mount point
	MOUNTPOINTS=(\
		"$SELFHOSTEDDIR" "$TARGETDIR"/repo\
		"$TESTFINALCONFIG" "$TARGETDIR"/config.yml\
		"$(dirname "$0")" "$TARGETDIR"/tests\
		"/root/Prog/pebble/test/certs/pebble.minica.pem" "$CONTAINERROOT"/usr/local/share/ca-certificates/pebble.minica.crt
	)
	doMount "${MOUNTPOINTS[@]}"

	# Run container setup script, to setup minimal environment for test (ansible, package cache, git)
	if ! runCMD "$CONTAINERNAME" setup lxc-attach --name "$CONTAINERNAME" -- /etc/ansible-selfhosted/tests/setup/container.sh; then
		maybeStop "$CONTAINERNAME"
		return 1
	fi

	export CONTAINERIP="$(lxc-attach --name "$CONTAINERNAME" -- ip a | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')"
	# Save CONTAINERNAME in DNS
	EXPECTCONFIG="address=/"$CONTAINERNAME"/"$CONTAINERIP""
	if ! grep -q "$EXPECTCONFIG" /etc/dnsmasq.conf; then
		debug "Configuring DNS records for "$CONTAINERNAME""
		if grep -q "address=/"$CONTAINERNAME"" /etc/dnsmasq.conf; then
			# update existing DNS config
			sed -i "s#address=/"$CONTAINERNAME".*\$#"$EXPECTCONFIG"#g" /etc/dnsmasq.conf
		else
			echo "$EXPECTCONFIG" >> /etc/dnsmasq.conf
		fi
		debug "Restarting DNS server for new records for "$CONTAINERNAME""
		systemctl restart dnsmasq
	fi

	# Run the recipe through ansible-selfhosted
	if ! runCMD "$CONTAINERNAME" recipe lxc-attach --name "$CONTAINERNAME" -- /etc/ansible-selfhosted/tests/helpers/ansible.sh /etc/ansible-selfhosted/config.yml /etc/ansible-selfhosted/repo; then
		maybeStop "$CONTAINERNAME"
		return 1
	fi

	# Run the actual tests
	if ! runCMD "$CONTAINERNAME" test "$TESTFILE"; then
		maybeStop "$CONTAINERNAME"
		return 1
	fi

	# Test was successful, stop and destroy the container
	doUnmount "${MOUNTPOINTS[@]}"
	lxc-stop "$CONTAINERNAME"
	lxc-destroy "$CONTAINERNAME"
	return 0
}

# Fallback for SELFHOSTEDDIR, which can be overridden by cli_flags
SELFHOSTEDDIR="/root/Prog/ansible-selfhosted"
# Fallback for DISTRO, which can be overriden by cli_flags
DISTRO="debian"
# By default, when no test is provided we find all tests available
# This is done later so CLI parsing can naively add tests it finds
TESTS=()

while [[ $# -gt 0 ]]; do
	case "$1" in
		-h|--help|help)
			help
			exit 0
			;;
		-b|--basedir)
			SELFHOSTEDDIR="$2"
			shift 2
			;;
		-d|--distro)
			DISTRO="$2"
			shift 2
			;;
		--debug|-v|--verbose)
			DEBUG=1
			shift
			;;
		*)
			if [ ! -f "$1" ]; then
				red "ERROR: Cannot find test file "$1""
				echo "Please give test.sh path to valid test/NAME.sh as arguments"
				echo "Alternatively, don't give any arguments at all and all tests are run"
				exit 1
			fi
			TESTS+=("$1")
			shift 1
			;;
	esac
done

# If no tests were asked for, find all of them
if [ ${#TESTS[@]} -eq 0 ]; then
	TESTS=("$(dirname "$0")"/tests/*.sh);
fi

# Sanitation:
#   - ensure LXC is setup
#   - ensure $DISTRO is either debian or archlinux
#   - ensure ansible-selfhosted directory ($SELFHOSTEDDIR) exists

if [ ! -d /var/lib/lxc ]; then
	red "ERROR: the test system is not setup yet. Please run setup/host.sh first."
	echo "Warning: host.sh will massively change network setup, in particular in regards to networking."
	echo "You may lose internet access as a result if you don't setup the good settings in there."
	echo "Alternatively, you can setup LXC according to your desires. If /var/lib/lxc is found, it will be used as such."
	exit 1
fi

case "$DISTRO" in
	"debian"|"archlinux")
		;;
	"deb")
		DISTRO="debian"
		;;
	"arch")
		DISTRO="arch"
		;;
	*)
		red "ERROR: unsupported distro $DISTRO"
		exit 1
		;;
esac

if [ ! -d "$SELFHOSTEDDIR" ]; then
	red "ERROR: Please give me a path to a valid ansible-selfhosted repo"
	echo "The script automatically tries /root/Prog/ansible-selfhosted, so you can just:"
	echo "  mkdir -p /root/Prog && cd /root/Prog && git clone https://codeberg.org/southerntofu/ansible-selfhosted"
	exit 1
fi

# Parameter sanitation is done!
# Register common environment variables
export HOSTTESTS="$(dirname "$0")"
export HOSTIMPL="$SELFHOSTEDDIR"
export HOSTIP="$(ip a | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' |grep -v '127.0.0.1')"

# OK now we want to start the container, copy things and start the tests

echo "Using ansible-selfhosted from "$SELFHOSTEDDIR""

RESULTS_NAME=()
RESULTS_CODE=()
RESULTS_OUTPUT=()
HAS_FAILED=0
for entry in "${TESTS[@]}"; do
	TESTFILE="$entry"
	TESTNAME="$(echo -n "$TESTFILE" | sed 's/.*\?\/\?tests\///')"
	TESTCONF="$(echo -n "$TESTFILE" | sed 's/.sh$/.yml/')"
	debug "$TESTNAME ($TESTFILE)"
	RESULTS_NAME+=("$TESTNAME")
	if [ ! -x "$TESTFILE" ]; then
		RESULTS_OUTPUT+=("ERROR: "$(realpath "$TESTFILE")" is not executable")
		RESULTS_CODE+=(2)
		HAS_FAILED=1
		echo "skip "$TESTNAME""
		continue
	elif [ ! -f "$TESTCONF" ]; then
		RESULTS_OUTPUT+=("ERROR: "$(realpath "$TESTCONF")" configuration does not exist")
		RESULTS_CODE+=(2)
		HAS_FAILED=1
		echo "skip "$TESTNAME""
		continue
	else
		# runTest TESTNAME TESTFILE TESTCONF
		if ! runTest "$TESTNAME" "$TESTFILE" "$TESTCONF"; then
			HAS_FAILED=1
			red "fail "$TESTNAME""
		else
			green "ok "$TESTNAME""
		fi
	fi
done

if [ ! $HAS_FAILED -eq 0 ]; then
	mkdir -p /tmp/selfhosted-tests
	red "ERROR: Some tests have failed."
	red "Below you will find the containers who failed, and the log files containing more information (on the host):"
	i=0
	while [[ $i -lt ${#RESULTS_CODE[@]} ]]; do
		if [ ! ${RESULTS_CODE[i]} -eq 0 ] || [ $DEBUG -eq 1 ]; then
			if [[ "$(echo "${RESULTS_OUTPUT[i]}" | wc -l)" -gt 10 ]]; then
				echo "${RESULTS_OUTPUT[i]}" > /tmp/selfhosted-tests/$TESTNAME.log
				red "  - $CONTAINERPREFIX-$TESTNAME -> /tmp/selfhosted-tests/$TESTNAME.log"
			else
				red "  - $CONTAINERPREFIX-$TESTNAME -> OUTPUT BELOW:"
				echo "${RESULTS_OUTPUT[i]}" | sed 's/^/        /g'
			fi
		fi
		i=$((i+1))
	done

	red "\nThe containers have been stopped, but you can access them with lxc-start NAME && lxc-attach NAME"
	exit 1
fi

green "SUCCESS all tests passed"
