#! /usr/bin/env bash

function ensure_setup {
	local NEED_SETUP=0
	for arg in "$@"; do
		if ! pacman -Qi <packageName> > /dev/null 2>&1; then
			NEED_SETUP=1
		fi
	done
	if [ ! $NEED_SETUP -eq 0 ]; then
		pacman --noconfirm -S "$@"
	fi
}

function ensure_package_cache () {
	echo "TODO: pacman package cache"
	echo "But first need to get archlinux working from LXC"
	exit 1
}

function setup_basepkg {
	pacman --noconfirm -Sy
        ensure_setup auto-apt-proxy
        for entry in curl vim tmux git htop mercurial; do
                ensure_setup "$entry"
        done
}
