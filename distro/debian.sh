#! /usr/bin/env bash

function ensure_setup {
	if ! dpkg -s "$@" >/dev/null 2>&1; then
		apt-get --assume-yes install "$@"
	fi
}

function ensure_package_cache () {
	if getent hosts apt-proxy; then
		echo "Acquire::http::Proxy \"http://apt-proxy:3142\";" > /etc/apt/apt.conf.d/proxy.conf
	else
		echo "WARNING: You are not using a local package cache"
		echo "I will prevent you from doing that, but if you REALLY WANT TO (it will eat your internet!)"
		echo "you can always remove the exit statement below in this file ($0)"
		exit 1
	fi
}

function setup_basepkg {
	apt-get --assume-yes update
        #ensure_setup auto-apt-proxy
	ensure_setup curl vim tmux git htop mercurial iputils-ping earlyoom
	systemctl enable earlyoom
}

# Specifically for setup/host.sh

function setup_golang {
	if [ ! -d /usr/local/go ]; then
		echo "[golang] download"
		tmpfile="$(mktemp --suffix .tar.gz)"
		echo "$tmpfile"
		curl --location "https://go.dev/dl/go1.17.8.linux-amd64.tar.gz" > "$tmpfile"
		tar -C /usr/local -xzf "$tmpfile"
	fi
	if ! go version >/dev/null 2>&1; then
		if ! grep -q "/usr/local/go/bin" /etc/profile; then
			echo "[golang] add to \$PATH"
			echo "export PATH=\$PATH:/usr/local/go/bin" >> /etc/profile
			sync
		fi
		source /etc/profile
        echo "WARNING: You need to restart your session for golang to be added to PATH"
	fi
	go version > /dev/null
    export GOPATH="/root/go"
}

function setup_acme_server() {
	setup_golang
	if ! which pebble > /dev/null; then
		if [ -f "$GOPATH"/bin/pebble ]; then
		    ln -s "$GOPATH"/bin/pebble /usr/local/bin/pebble
		else
		    ORIGDIR="$(pwd)"
		    cd "$SRCDIR"

		    if [ ! -d pebble ]; then
			if ! git clone https://github.com/letsencrypt/pebble; then
			    echo "Failed to download pebble"
			    cd "$ORIGDIR"
			    exit 1
			fi
		    fi
		    cd pebble
		    if ! go install ./...; then
			echo "Failed to build pebble"
			cd "$ORIGDIR"
			exit 1
		    fi
		    cd "$ORIGDIR"
		fi
	fi
	if ! which pebble > /dev/null; then
		echo "ERROR: Failed to install pebble"
		exit 1
	fi
	if [ ! -d /etc/pebble ]; then
		mkdir /etc/pebble
	fi
	[ ! -f /etc/pebble/cert.pem ] && cp "$SRCDIR"/pebble/test/certs/localhost/cert.pem /etc/pebble/cert.pem
	[ ! -f /etc/pebble/key.pem ] && cp "$SRCDIR"/pebble/test/certs/localhost/key.pem /etc/pebble/key.pem
	if [ ! -f /etc/pebble/config.json ]; then
		cat > /etc/pebble/config.json << EOF
{
  "pebble": {
      "listenAddress": "0.0.0.0:14000",
          "managementListenAddress": "0.0.0.0:15000",
    "certificate": "/etc/pebble/cert.pem",
    "privateKey": "/etc/pebble/key.pem",
    "httpPort": 80,
    "tlsPort": 443,
    "ocspResponderURL": "",
    "externalAccountBindingRequired": false
  }
}
EOF
	fi
	cat > /etc/systemd/system/pebble.service << EOF
[Unit]
Description=pebble
After=network.target

[Service]
ExecStart=/usr/local/bin/pebble -config /etc/pebble/config.json
User=root
Group=root
Restart=on-failure
RestartSec=5s
Environment="PEBBLE_VA_NOSLEEP=1"

[Install]
WantedBy=multi-user.target
EOF
	systemctl daemon-reload
	systemctl start pebble
	systemctl enable pebble
}

function ensure_pebble_ca() {
	if [ ! -f /usr/local/share/ca-certificates/pebble.minica.crt ]; then
		echo "Missing pebble CA root certificate for TLS tests"
		return 1
	fi
	update-ca-certificates 2>&1 > /dev/null
	if ! grep -q "pebble" /etc/hosts; then
		echo "$HOSTIP pebble" >> /etc/hosts
	fi
}
