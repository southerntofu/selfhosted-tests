#! /usr/bin/env bash

# Load settings
CFGFILE="$(dirname "$0")"/../config.env
if [ ! -f "$CFGFILE" ]; then
	echo "Welcome to the ansible-selfhosted test suite!"
	echo "Please rename config.default.env to config.env and edit it to match your local configuration."
	echo "WARNING: Running this script again after setting the correct settings will change the network configuration on your machine."
	echo "It should be run on a machine dedicated to tests with a fresh Debian, unless you absolutely know what you are doing."
	exit 1
fi

source "$(dirname "$0")"/../config.env
source "$(dirname "$0")"/../helpers/common.sh


# SETTINGS
HOSTMAC="AC:AB:13:37:42:01"
MACADDRESSES="AC:AB:13:37:01:xx"
BRIDGEDINTERFACE="enp2s0"

set -e
#set -x

function setup_bridge_interface() {
	local CONFFILE="$1"
	ensure_config "$CONFFILE" "auto" "br0" " "
	ensure_config "$CONFFILE" "iface" "br0 inet dhcp" " "
	ensure_config "$CONFFILE" "    hwaddress" "ether $HOSTMAC" " "
	ensure_config "$CONFFILE" "    bridge_ports" "$BRIDGEDINTERFACE" " "
	ensure_config "$CONFFILE" "    bridge_fd" "0" " " 
	ensure_config "$CONFFILE" "    bridge_maxwait" "0" " "
}

# This function automatically creates a br0 bridge to the $BRIDGEDINTERFACE
function setup_bridge() {
	local CONFFILE="/etc/network/interfaces.d/bridge.conf"
	local RELOAD=0
	if [ ! -f "$CONFFILE" ]; then
		RELOAD=1
		echo "[bridge] create bridge br0 to $BRIDGEDINTERFACE (host: $HOSTMAC, children: $MACADDRESSES)"
		#echo -ne "$EXPECTEDCONF" > "$CONFFILE"
		setup_bridge_interface "$CONFFILE"
	else
		local EXPECTEDCONF="$(mktemp)"
		setup_bridge_interface "$EXPECTEDCONF" > /dev/null
		if [[ "$(cat "$EXPECTEDCONF")" != "$(cat "$CONFFILE")" ]]; then
			RELOAD=1
			echo "[bridge] override previous configuration from $CONFFILE:"
                        cat "$CONFFILE" | sed 's/^/    /g'
			echo "[bridge] set bridge br0 to $BRIDGEDINTERFACE (host: $HOSTMAC, children: $MACADDRESSES)"
			cp "$EXPECTEDCONF" "$CONFFILE"
		fi

		rm "$EXPECTEDCONF"
		#echo -ne "$EXPECTEDCONF" > "$CONFFILE"
		#setup_bridge_interface "$CONFFILE"
	fi

	# Now check that the bridged interface is no longer configured
	# because host IP address will be acquired via DHCP on the br0 interface
	# Only works with a default "dhcp" configuration, otherwise you'll have to edit manually
	if ! grep -q "iface $BRIDGEDINTERFACE inet manual" /etc/network/interfaces; then
		RELOAD=1
		echo "[bridge] set $BRIDGEDINTERFACE to manual"
		sed -i "s/iface $BRIDGEDINTERFACE inet dhcp/iface $BRIDGEDINTERFACE inet manual/" /etc/network/interfaces
	fi

	if [ $RELOAD -eq 1 ]; then
		echo "[bridge] restart networking"
		systemctl restart networking
	fi
}

# First setup LXC so we can deploy a fresh debian container
function setup_lxc() {
    ensure_setup lxc libvirt0 libpam-cgfs bridge-utils uidmap

    ensure_config "/etc/default/lxc-net" "USE_LXC_BRIDGE" "false"
    ensure_config "/etc/lxc/default.conf" "lxc.net.0.hwaddr" "$MACADDRESSES"
    ensure_config "/etc/lxc/default.conf" "lxc.net.0.type" "veth"
    ensure_config "/etc/lxc/default.conf" "lxc.net.0.link" "br0"
    ensure_config "/etc/lxc/default.conf" "lxc.net.0.flags" "up"
    # Doesn't work below? Probably some cgroups to configure on host
    #ensure_config "/etc/lxc/default.conf" "lxc.cgroup.cpuset.cpus" "1"
    #ensure_config "/etc/lxc/default.conf" "lxc.cgroup.cpuset.mems" "1"
    #ensure_config "/etc/lxc/default.conf" "lxc.cgroup.memory.swappiness" "0"
    #ensure_config "/etc/lxc/default.conf" "lxc.cgroup.memory.limit_in_bytes" "512M"
}

# Setup local DNS resolver which will know about LXC containers
# via /etc/hosts send sighup to dnsmasq to reload /etc/hosts
function setup_dns() {
	ensure_setup dnsmasq
	ip="$(cat /var/lib/dhcp/dhclient.leases | grep dhcp-server-identifier | grep -oP "[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}")"
	#sed -i "s#nameserver .*$#nameserver 127.0.0.1#g" /etc/resolv.conf
	#ensure_config "/etc/resolv.conf" "nameserver" "127.0.0.1" " "
	if ! grep -q "nameserver 127.0.0.1" /etc/resolv.conf; then
		ensure_config "/etc/dhcp/dhclient.conf" "supersede" "domain-name-servers 127.0.0.1;" " ";
		systemctl restart networking
	fi
	ensure_config "/etc/dnsmasq.conf" "no-resolv" "" " "
	ensure_config "/etc/dnsmasq.conf" "server" "$ip" "="
	systemctl restart dnsmasq
	systemctl enable dnsmasq
}

setup_basepkg
ensure_package_cache
setup_acme_server
setup_bridge
setup_lxc
setup_dns
