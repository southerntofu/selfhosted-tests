#! /usr/bin/env bash

set -e

source "$(dirname "$0")"/../helpers/common.sh

export SRCDIR="/root/Prog"
if [ ! -d "$SRCDIR" ]; then
	mkdir "$SRCDIR"
fi

ensure_network 10
ensure_package_cache
setup_basepkg
# Don't use tor for git, we now run all tests locally
#setup_tor
ensure_setup ansible
ensure_pebble_ca
#setup_ansible_selfhosted
